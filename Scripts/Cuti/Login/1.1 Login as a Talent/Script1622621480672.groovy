import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'open browser'
WebUI.openBrowser('')

WebUI.navigateToUrl('https://matrix-2.cloudias79.com/#/')

WebUI.maximizeWindow()

'verify element'
WebUI.verifyElementText(findTestObject('Login/Page_Leave Management System 79/p_Leave Management System 79'), 'Leave Management System 79')

WebUI.verifyElementText(findTestObject('Login/Page_Leave Management System 79/p_Login Here'), 'Login Here !')

'input username'
WebUI.setText(findTestObject('Login/Page_Leave Management System 79/input_Log in_horizontal_login_userName'), 'talent')

'input password'
WebUI.setEncryptedText(findTestObject('Login/Page_Leave Management System 79/input_Log in_horizontal_login_password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Login/Page_Leave Management System 79/button_Log in'))

WebUI.verifyElementText(findTestObject('Login/Page_Leave Management System 79/p_Cuti anda tersisa'), 'Cuti anda tersisa')

WebUI.verifyElementText(findTestObject('Login/Page_Leave Management System 79/button_Buat Pengajuan Cuti'), 'Buat Pengajuan Cuti')

WebUI.verifyTextPresent('Success', false)

WebUI.closeBrowser()

