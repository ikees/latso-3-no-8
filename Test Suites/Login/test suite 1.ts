<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>test suite 1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>false</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a74dc63d-7f98-4024-865e-e1db5cc56a15</testSuiteGuid>
   <testCaseLink>
      <guid>739b13d1-bcff-43f4-a646-57cdd173f59c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cuti/Login/1.1 Login as a Talent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d80730b6-0dcf-4be7-85a3-9453201a8d7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Cuti/Login/1.2 login valid username and invalid password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19c161b5-964e-4b7d-a057-52fd3a2bd799</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Cuti/Login/1.3 Login invalid username and valid password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0f17abb-864b-4a65-82c6-af431bb3cf41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Cuti/Login/1.4 Login valid username and blank password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e6415ec-b9a1-44ad-8322-3d84245e977a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Cuti/Login/1.5 Login blank username and valid password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f20a4ec-b2f1-4e06-b8f3-d5d3867100cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Cuti/Login/1.6 Login blank username and blank password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>074a325c-c496-46ea-b0bd-9e0a7892ddf4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Cuti/Login/1.7 Login as a spv</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f3fb5f3-f28a-4e3c-8d54-1de1bd85d940</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Cuti/Login/1.8 Login as a HRD</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
